#ifndef LOGGER_H
#define LOGGER_H
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

enum Level{DEBUG, INFO, WARN, ERROR, FATAL, UNKNOWN};

void logger(enum Level level, const char* message);
void logger_set_threshold(enum Level threshold);

#endif
