#include "logger.h"

enum Level logger_threshold = ERROR;

void logger(enum Level level, const char* message){
	if(level < logger_threshold)
		return;
	time_t rawtime;
	struct tm * timeinfo;
	time ( &rawtime );
	timeinfo = localtime ( &rawtime );
	char* ctime = asctime(timeinfo);
	ctime[strlen(ctime) - 1] = 0;
	if(isatty(2)){
		switch(level){
			case FATAL:
				fprintf(stderr, "\e[1m\e[31m[%s] %s\n\e[0m", ctime, message);
				break;
			case ERROR:
				fprintf(stderr, "\e[31m[%s] %s\n\e[0m", ctime, message);
				break;
			case WARN:
			case INFO:
			default:
				fprintf(stderr, "[%s] %s\n", ctime, message);
				break;
		}
	}
	else{
		fprintf(stderr, "[%s] %s\n", ctime, message);
	}
}


void logger_set_threshold(enum Level thresh){
	logger_threshold = thresh;
}
